package com.example.examenc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActividad extends AppCompatActivity {
    private EditText etBase, etAltura;
    private TextView lblCalArea, lblCalPerimetro, txtNombre;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo_actividad);


        etBase = findViewById(R.id.etBase);
        etAltura = findViewById(R.id.etAltura);
        lblCalArea = findViewById(R.id.lblCalArea);
        lblCalPerimetro = findViewById(R.id.lblCalPerimetro);
        txtNombre = findViewById(R.id.txtNombre);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Recibe el nombre de usuario desde el Intent
        String nombre = getIntent().getStringExtra("nombre");
        txtNombre.setText("Mi nombre es: " + nombre);


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    int base = Integer.parseInt(etBase.getText().toString());
                    int altura = Integer.parseInt(etAltura.getText().toString());

                    Rectangulo rectangulo = new Rectangulo(base, altura);

                    float area = rectangulo.calcularArea();
                    float perimetro = rectangulo.calcularPerimetro();

                    lblCalArea.setText("Área: " + area);
                    lblCalPerimetro.setText("Perímetro: " + perimetro);
                } else {
                    Toast.makeText(RectanguloActividad.this, "Todos los campos son requeridos y deben ser válidos", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarCampos() {
        return !etBase.getText().toString().isEmpty() &&
                !etAltura.getText().toString().isEmpty();
    }

    private void limpiarCampos() {
        etBase.setText("");
        etAltura.setText("");
        lblCalArea.setText("Área:");
        lblCalPerimetro.setText("Perímetro:");
    }
}
